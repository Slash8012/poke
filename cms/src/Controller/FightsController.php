<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\Poke;

/**
 * Fights Controller
 *
 * @property \App\Model\Table\FightsTable $Fights
 *
 * @method \App\Model\Entity\Fight[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FightsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs'],
        ];
        $fights = $this->paginate($this->Fights);

        $this->set(compact('fights'));
    }

    /**
     * View method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fight = $this->Fights->get($id, [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs'],
        ]);

        $this->set('fight', $fight);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fight = $this->Fights->newEntity();
        if ($this->request->is('post')) {
            $formData = $this->request->getData();
            $formData['winner_dresseur_id'] = $this->_retrieveFightWinner($formData);
            if ($formData['winner_dresseur_id']) {
                 $fight = $this->Fights->patchEntity($fight, $formData);
            }
           // $this->Fights->save($fight);
            if ($this->Fights->save($fight)) {
                $this->Flash->success(__('The fight has been saved.'));

                return $this->redirect(['action' => 'view',$fight->id]);
            }
            $this->Flash->error(__('The fight could not be saved. Please, try again.'));
        }
        $firstDresseurs = $this->Fights->FirstDresseurs->find('list', ['limit' => 200]);
        $secondDresseurs = $this->Fights->SecondDresseurs->find('list', ['limit' => 200]);
       // $winnerDresseurs = $this->Fights->WinnerDresseurs->find('list', ['limit' => 200]);
        $this->set(compact('fight', 'firstDresseurs', 'secondDresseurs', 'winnerDresseurs'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    /*
    public function edit($id = null)
    {
        $fight = $this->Fights->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $fight = $this->Fights->patchEntity($fight, $this->request->getData());
            if ($this->Fights->save($fight)) {
                $this->Flash->success(__('The fight has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The fight could not be saved. Please, try again.'));
        }
        $firstDresseurs = $this->Fights->FirstDresseurs->find('list', ['limit' => 200]);
        $secondDresseurs = $this->Fights->SecondDresseurs->find('list', ['limit' => 200]);
        $winnerDresseurs = $this->Fights->WinnerDresseurs->find('list', ['limit' => 200]);
        $this->set(compact('fight', 'firstDresseurs', 'secondDresseurs', 'winnerDresseurs'));
    }
    */

    protected function _retrieveFightWinner($formData) 
    {
        /*
        $nbtourj1 = rand(1,20); // nombre aleatoire entre 1 et 20 compris (normalement)
        $nbtourj2 = rand(1,20);
        $hppoke1 = 100;
        $hppoke2 = 100;
        
        $attackj1 =  $hppoke2/$nbtourj1;
        $attackj2 =  $hppoke1/$nbtourj2;

        if ($attackj1 > $attackj2)
        {
             $formData['winner_dresseur_id'] = $formData['first_dresseur_id'];
        }
        else
        {
             $formData['winner_dresseur_id'] = $formData['second_dresseur_id'];
        }
        return $formData['winner_dresseur_id'];
        */
        
        $ordre = rand(0,1);
        $mort = 0;
        $dsn = 'mysql:dbname=cake;host=127.0.0.1';
        $user = 'root';
        $password = 'alex80.12';
        $bdd = new PDO($dsn,$user,$password);
        
        $requete = $bdd->prepare("select poke_id from dresseur_pokemons where dresseur_id = :id");
        $requete->execute(array(':id' => $formData['first_dresseur_id']));
        $IdPokeOne = $requete -> fetch(PDO::FETCH_NUM);
        $IdPokeOne = $IdPokeOne[0];

        $requete = $bdd->prepare("select poke_id from dresseur_pokemons where dresseur_id = :id");
        $requete->execute(array(':id' => $formData['second_dresseur_id']));
        $IdPokeTwo = $requete -> fetch(PDO::FETCH_NUM);
        $IdPokeTwo = $IdPokeTwo[0];

        $requete = $bdd->prepare("select Health from pokes where id=:id");
        $requete->execute(array(':id' => $IdPokeOne));
        $PokeHPOne = $requete -> fetch(PDO::FETCH_NUM);
        $PokeHPOne = $PokeHPOne[0];

        $requete = $bdd->prepare("select Health from pokes where id=:id");
        $requete->execute(array(':id' => $IdPokeTwo));
        $PokeHPTwo = $requete -> fetch(PDO::FETCH_NUM);
        $PokeHPTwo = $PokeHPTwo[0];

        $requete = $bdd->prepare("select Attack from pokes where id=:id");
        $requete->execute(array(':id' => $IdPokeOne));
        $PokeAttackOne = $requete -> fetch(PDO::FETCH_NUM);
        $PokeAttackOne = $PokeAttackOne[0];

        $requete = $bdd->prepare("select Attack from pokes where id=:id");
        $requete->execute(array(':id' => $IdPokeTwo));
        $PokeAttackTwo = $requete -> fetch(PDO::FETCH_NUM);
        $PokeAttackTwo = $PokeAttackTwo[0];

        $requete = $bdd->prepare("select Defense from pokes where id=:id");
        $requete->execute(array(':id' => $IdPokeOne));
        $PokeDefenseOne = $requete -> fetch(PDO::FETCH_NUM);
        $PokeDefenseOne = $PokeDefenseOne[0];

        $requete = $bdd->prepare("select Defense from pokes where id=:id");
        $requete->execute(array(':id' => $IdPokeTwo));
        $PokeDefenseTwo = $requete -> fetch(PDO::FETCH_NUM);
        $PokeDefenseTwo = $PokeDefenseTwo[0];

        while ($mort ==0)
        {
            if ($ordre == 0 and $mort == 0)
            {
                if(($PokeAttackOne - $PokeDefenseTwo) > 0)
                {
                    $PokeHPTwo = $PokeHPTwo - ($PokeAttackOne - $PokeDefenseTwo);
                }
                else
                {
                    $PokeHPTwo = $PokeHPTwo-1;
                }

                if($PokeHPTwo <= 0) {$mort = 1;} //mort si plus de pv 

                if ($mort == 0)
                {
                    if(($PokeAttackTwo - $PokeDefenseOne) > 0)
                    {
                        $PokeHPOne = $PokeHPOne - ($PokeAttackTwo - $PokeDefenseOne);
                    }
                    else
                    {
                        $PokeHPOne = $PokeHPOne - 1;
                    }   
                    if ($PokeHPOne<=0) {$mort=1;} //mort si plus de pv
                }
            }

            if ($ordre == 1 and $mort == 0)
            {
                if(($PokeAttackTwo - $PokeDefenseOne) > 0)
                {
                    $PokeHPOne = $PokeHPOne - ($PokeAttackTwo - $PokeDefenseOne);
                }
                else
                {
                    $PokeHPOne = $PokeHPOne -1;
                }

                if($PokeHPOne <= 0){$mort =1;}

                if ($mort == 0) //tour de l'autre joueur
                {
                    if(($PokeAttackOne - $PokeDefenseTwo) > 0)
                    {
                        $PokeHPTwo = $PokeHPTwo - ($PokeAttackOne - $PokeDefenseTwo);
                    }
                    else
                    {
                        $PokeHPTwo = $PokeHPTwo -1;
                    }
                }
            }
        }

        if ($PokeHPOne <=0) 
        {
            $formData['winner_dresseur_id'] = $formData['second_dresseur_id'];
        }
        else
        {
            $formData['winner_dresseur_id'] = $formData['first_dresseur_id'];
        }
        return $formData['winner_dresseur_id'];
        
    }




    /**
     * Delete method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fight = $this->Fights->get($id);
        if ($this->Fights->delete($fight)) {
            $this->Flash->success(__('The fight has been deleted.'));
        } else {
            $this->Flash->error(__('The fight could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
