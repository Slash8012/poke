<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DresseurPokemons Controller
 *
 * @property \App\Model\Table\DresseurPokemonsTable $DresseurPokemons
 *
 * @method \App\Model\Entity\DresseurPokemon[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DresseurPokemonsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dresseurs', 'Pokes']
        ];
        $dresseurPokemons = $this->paginate($this->DresseurPokemons);

        $this->set(compact('dresseurPokemons'));
    }

    /**
     * View method
     *
     * @param string|null $id Dresseur Pokemon id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dresseurPokemon = $this->DresseurPokemons->get($id, [
            'contain' => ['Dresseurs', 'Pokes']
        ]);

        $this->set('dresseurPokemon', $dresseurPokemon);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dresseurPokemon = $this->DresseurPokemons->newEntity();
        if ($this->request->is('post')) {
            $dresseurPokemon = $this->DresseurPokemons->patchEntity($dresseurPokemon, $this->request->getData());
            if ($this->DresseurPokemons->save($dresseurPokemon)) {
                $this->Flash->success(__('The dresseur pokemon has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dresseur pokemon could not be saved. Please, try again.'));
        }
        $dresseurs = $this->DresseurPokemons->Dresseurs->find('list', ['limit' => 200]);
        $pokes = $this->DresseurPokemons->Pokes->find('list', ['limit' => 200]);
        $this->set(compact('dresseurPokemon', 'dresseurs', 'pokes'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Dresseur Pokemon id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dresseurPokemon = $this->DresseurPokemons->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dresseurPokemon = $this->DresseurPokemons->patchEntity($dresseurPokemon, $this->request->getData());
            if ($this->DresseurPokemons->save($dresseurPokemon)) {
                $this->Flash->success(__('The dresseur pokemon has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dresseur pokemon could not be saved. Please, try again.'));
        }
        $dresseurs = $this->DresseurPokemons->Dresseurs->find('list', ['limit' => 200]);
        $pokes = $this->DresseurPokemons->Pokes->find('list', ['limit' => 200]);
        $this->set(compact('dresseurPokemon', 'dresseurs', 'pokes'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Dresseur Pokemon id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dresseurPokemon = $this->DresseurPokemons->get($id);
        if ($this->DresseurPokemons->delete($dresseurPokemon)) {
            $this->Flash->success(__('The dresseur pokemon has been deleted.'));
        } else {
            $this->Flash->error(__('The dresseur pokemon could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
