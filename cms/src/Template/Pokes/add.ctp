<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Poke $poke
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Pokes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dresseur'), ['controller' => 'Dresseurs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dresseur Pokemons'), ['controller' => 'DresseurPokemons','action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dresseur Pokemons'), ['controller' => 'DresseurPokemons','action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Fight'), ['controller' => 'Fights', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Fight'), ['controller' => 'Fights', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="pokes form large-9 medium-8 columns content">
    <?= $this->Form->create($poke) ?>
    <fieldset>
        <legend><?= __('Add Poke') ?></legend>
        <?php
            echo $this->Form->control('Nom');
            echo $this->Form->control('pokedex_number');
            echo $this->Form->control('health');
            echo $this->Form->control('attack');
            echo $this->Form->control('defense');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
