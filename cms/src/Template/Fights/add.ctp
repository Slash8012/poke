<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fight $fight
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Pokemon'), ['controller' => 'Pokes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pokemon'), ['controller' => 'Pokes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dresseur'), ['controller' => 'Dresseurs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['controller'=> 'Dresseurs','action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dresseur Pokemon'), ['controller' => 'DresseurPokemons', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dresseur Pokemons'), ['controller' => 'DresseurPokemons', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Fight'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="fights form large-9 medium-8 columns content">
    <?= $this->Form->create($fight) ?>
    <fieldset>
        <legend><?= __('Add Fight') ?></legend>
        <?php
            echo $this->Form->control('first_dresseur_id');
            echo $this->Form->control('second_dresseur_id');
            //echo $this->Form->control('winner_dresseur_id');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
