<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fight $fight
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $fight->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $fight->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Fights'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="fights form large-9 medium-8 columns content">
    <?= $this->Form->create($fight) ?>
    <fieldset>
        <legend><?= __('Edit Fight') ?></legend>
        <?php
            echo $this->Form->control('first_dresseur_id');
            echo $this->Form->control('second_dresseur_id');
            echo $this->Form->control('winner_dresseur_id');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
