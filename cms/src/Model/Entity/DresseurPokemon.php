<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DresseurPokemon Entity
 *
 * @property int $id
 * @property int $dresseur_id
 * @property int $poke_id
 * @property bool $favoris
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Dresseur $dresseur
 * @property \App\Model\Entity\Poke $poke
 */
class DresseurPokemon extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'dresseur_id' => true,
        'poke_id' => true,
        'favoris' => true,
        'created' => true,
        'modified' => true,
        'dresseur' => true,
        'poke' => true
    ];
}
